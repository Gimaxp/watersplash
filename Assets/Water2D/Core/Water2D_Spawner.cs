﻿namespace Water2D
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine.UI;
    using DynamicLight2D;
    using DG.Tweening;
    public struct microSpawn
    {
        public Vector3 pos;
        public int amount;
        public Vector2 initVel;

        public microSpawn(Vector3 pos, int amount, Vector2 initVel)
        {
            this.pos = pos;
            this.amount = amount;
            this.initVel = initVel;
        }
    }

    public class Water2D_Spawner : MonoBehaviour
    {

        public static Water2D_Spawner instance;

        void Awake()
        {
            if (instance == null)
                instance = this;

        }

        [Title("Water 2D", 20f, 20)]

        [Space(25f)]

        /// <summary>
        /// Drops objects array.
        /// </summary>
        public GameObject[] WaterDropsObjects;

        /// <summary>
        /// The size of each drop.
        /// </summary>
        [Range(0f, 2f)] public float size = .45f;

        /// <summary>
        /// The life time of each particle.
        /// </summary>
        [Range(0f, 100f)] public float LifeTime = 5f;

        /// <summary>
        /// The delay between particles emission.
        /// </summary>
        [Range(0f, 5f)] public float DelayBetweenParticles = 0.05f;

        /// <summary>
        /// The water material.
        /// </summary>

        [Header("Material & color")]
        public Material WaterMaterial;
        public Color FillColor = new Color(0f, 112 / 255f, 1f);
        public Color StrokeColor = new Color(4 / 255f, 156 / 255f, 1f);



        [Separator()]

        [Header("Speed & direction")]
        /// <summary>
        /// The initial speed of particles after spawn.
        /// </summary>
        public Vector2 initSpeed = new Vector2(1f, -1.8f);

        [Header("Напор по Y")]
        private float NaporY = 0;
        [Separator()]

        [Header("Apply setup changes over lifetime")]
        /// <summary>
        /// The dynamic changes can be apply ?.
        /// </summary>
        public bool DynamicChanges = true;

        [Space(20f)]
        [Header("Runtime actions")]

        [ButtonAttribute("Start!", "Water2D.Water2D_Spawner", "RunSpawner")] public bool btn_0;
        static void RunSpawner()
        {
            instance.Spawn();

        }
        [ButtonAttribute("Stop and restore", "Water2D.Water2D_Spawner", "StopSpawner")] public bool btn_1;
        static void StopSpawner()
        {
            instance.Restore();

        }

        [Separator()]

        [ButtonAttribute("Help?", "Water2D.Water2D_Spawner", "askHelp")] public bool btn;
        static void askHelp()
        {
            string email = "info@2ddlpro.com";
            string subject = "Water 2D Help!";
            Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + "");
        }



        bool _dynamic = true;
        public bool Dynamic
        {
            get { return _dynamic; }
            set
            {
                _dynamic = value;
            }
        }

        //bool alreadySpawned = false;



        public int AllBallsCount { get; private set; }
        public bool IsWaterInScene { get; private set; }

        //int usableDropsCount;
        int DefaultCount;


        // MICRO SPWNS
        // Used to make spawn in other positions with same properties (use same array of particles)
        List<microSpawn> microSpawns;

        bool _breakLoop = false;

        GameObject _parent;
        [SerializeField] private GameObject[] StartWoterSpawn;
        private List<Vector3> PosKranikod = new List<Vector3>();
        void Start()
        {
            //Application.targetFrameRate = 60;
            _parent = new GameObject("_metaBalls");
            _parent.hideFlags = HideFlags.HideInHierarchy;
            WaterDropsObjects[0].transform.SetParent(_parent.transform);
            WaterDropsObjects[0].transform.localScale = new Vector3(size, size, 1f);
            WaterDropsObjects[0].GetComponent<MetaballParticleClass>().Active = false;
            for (int i = 0; i < StartWoterSpawn.Length; i++)
            {
                PosKranikod.Add(StartWoterSpawn[i].transform.eulerAngles);
            }

            for (int i = 1; i < WaterDropsObjects.Length; i++)
            {
                //WaterDropsObjects[i] = Instantiate(WaterDropsObjects[0], gameObject.transform.position, new Quaternion(0,0,0,0)) as GameObject;
                //WaterDropsObjects [i].GetComponent<MetaballParticleClass>().Active = false;
                //WaterDropsObjects [i].transform.SetParent (_parent.transform);
                //WaterDropsObjects [i].transform.localScale = new Vector3 (size, size, 1f);
                //            WaterDropsObjects[i].layer = WaterDropsObjects[0].layer;
                //WaterDropsObjects[i].SetActive(false);
            }

            WaterDropsObjects[0].SetActive(false);

            AllBallsCount = WaterDropsObjects.Length;


            microSpawns = new List<microSpawn>(5); // Up to 5 microspwawn


            //   instance.Spawn();
        }
        private void Update()
        {
            if (Input.GetMouseButton(0) && GameController.Instance.Stay == GameController.StayGame.Game)
            {
                size = 0.3f;
                instance.Spawn();


            }
            if (Input.GetMouseButtonDown(0) && GameController.Instance.Stay == GameController.StayGame.Game)
            {
                GameController.Instance.WaterAudioSpawn.Play();
                StartCoroutine(ScaleKran());

            }
            else if (Input.GetMouseButtonUp(0) && GameController.Instance.Stay == GameController.StayGame.Game)
            {
                 GameController.Instance.WaterAudioSpawn.Stop();
                StopAllCoroutines();
             
            }
        }
        private IEnumerator ScaleKran()
        {
            while (true)
            {
                for (int i = 0; i < StartWoterSpawn.Length; i++)
                {
                    StartWoterSpawn[i].transform.parent.transform.parent.localScale = new Vector3(1, 1.2f, 1);
                }
                yield return new WaitForSeconds(0.05f);
                for (int i = 0; i < StartWoterSpawn.Length; i++)
                {

                   
                  
                    StartWoterSpawn[i].transform.parent.transform.parent.localScale = new Vector3(1, 1, 1);
              

                }
                yield return new WaitForSeconds(0.05f);
            }
        }
        public void RunMicroSpawn(Vector3 pos, int amount, Vector2 initVel)
        {
            addMicroSpawn(pos, amount, initVel);
            executeMicroSpawns();
        }

        public void addMicroSpawn(Vector3 pos, int amount, Vector2 initVel)
        {
            microSpawns.Add(new microSpawn(pos, amount, initVel));
        }



        public void Spawn()
        {
            Spawn(DefaultCount);
        }

        public void Spawn(int count)
        {
            executeMicroSpawns();
            if (DelayBetweenParticles == 0f)
            {
                SpawnAll();
            }
            else
            {
                loop(gameObject.transform.position, initSpeed, count);
            }

        }

        public void SpawnAll()
        {
            SpawnAllParticles(gameObject.transform.position, initSpeed, DefaultCount);
        }

        public void Spawn(int count, Vector3 pos)
        {
            executeMicroSpawns();
            loop(pos, initSpeed, count);
        }

        public void Spawn(int count, Vector3 pos, Vector2 InitVelocity, float delay = 0f)
        {
            executeMicroSpawns();
            loop(pos, InitVelocity, count, delay);
        }

        void executeMicroSpawns()
        {
            if (microSpawns == null)
                return;

            if (microSpawns.Count > 0 && microSpawns.Capacity > 0)
            {
                for (int i = 0; i < microSpawns.Count; i++)
                {
                    DynamicChanges = false;
                    loop(microSpawns[i].pos, microSpawns[i].initVel, microSpawns[i].amount, 0f);
                }

                microSpawns.Clear();
            }
        }

        public void Restore()
        {

            IsWaterInScene = false;
            _breakLoop = true;

            microSpawns.Clear();


            for (int i = 0; i < WaterDropsObjects.Length; i++)
            {
                if (WaterDropsObjects[i].GetComponent<MetaballParticleClass>().Active == true)
                {
                    WaterDropsObjects[i].GetComponent<MetaballParticleClass>().Active = false;
                }
                WaterDropsObjects[i].GetComponent<MetaballParticleClass>().witinTarget = false;
            }




            gameObject.transform.localEulerAngles = Vector3.zero;
            initSpeed = new Vector2(0, -2f);

            DefaultCount = AllBallsCount;
        }

        private void loop(Vector3 _pos, Vector2 _initSpeed, int count = -1, float delay = 0f, bool waitBetweenDropSpawn = true)
        {

            for (int i = 0; i < 2; i++)
            {
                var water = Instantiate(WaterDropsObjects[0], gameObject.transform.position, new Quaternion(0, 0, 0, 0)) as GameObject;
                water.GetComponent<MetaballParticleClass>().Active = false;
                int random = Random.Range(0, StartWoterSpawn.Length);
                GameObject SpawnPos = StartWoterSpawn[random];

                water.transform.SetParent(_parent.transform);
                water.transform.localScale = new Vector3(size, size, 1f);


                water.transform.position = SpawnPos.transform.position;
                SpawnPos.transform.eulerAngles = new Vector3(PosKranikod[random].x, PosKranikod[random].y, PosKranikod[random].z + Random.Range(-25, 25));
                _breakLoop = false;

                IsWaterInScene = true;


                MetaballParticleClass MetaBall = water.GetComponent<MetaballParticleClass>();

                MetaBall.LifeTime = LifeTime;

                MetaBall.Active = true;
                MetaBall.witinTarget = false;

                water.GetComponent<Rigidbody2D>().velocity = SpawnPos.transform.right * 4;  //_initSpeed;
            }

        }


        void SpawnAllParticles(Vector3 _pos, Vector2 _initSpeed, int count = -1, float delay = 0f, bool waitBetweenDropSpawn = true)
        {

            //  water.layer = WaterDropsObjects[0].layer;
            var water = Instantiate(WaterDropsObjects[0], gameObject.transform.position, new Quaternion(0, 0, 0, 0)) as GameObject;
            water.GetComponent<MetaballParticleClass>().Active = false;
            water.transform.SetParent(_parent.transform);
            water.transform.localScale = new Vector3(size, size, 1f);
            IsWaterInScene = true;

            int auxCount = 0;

            MetaballParticleClass MetaBall = water.GetComponent<MetaballParticleClass>();


            MetaBall.LifeTime = LifeTime;
            water.transform.position = transform.position;
            MetaBall.Active = true;
            MetaBall.witinTarget = false;

            if (_initSpeed == Vector2.zero)
                _initSpeed = initSpeed;

            if (DynamicChanges)
            {
                _initSpeed = initSpeed;
                MetaBall.transform.localScale = new Vector3(size, size, 1f);
                SetWaterColor(FillColor, StrokeColor);
            }

            water.GetComponent<Rigidbody2D>().velocity = _initSpeed;

        }

        public void SetWaterColor(Color fill, Color stroke)
        {
            WaterMaterial.SetColor("_Color", fill);
            WaterMaterial.SetColor("_StrokeColor", stroke);

        }

    }

}