﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
public class GameController : Singleton<GameController>
{
    public enum StayGame
    {
        Game,
        Lose,
        Win
    }
    public StayGame Stay = StayGame.Game;
    [SerializeField] private GameObject[] Levels;
    private int CurrentLevels;
    [SerializeField] private Sprite[] BollImages;
    [SerializeField] private List<GameObject> FinisObjectStart = new List<GameObject>();
    public List<GameObject> AllFinishObject = new List<GameObject>();
    [SerializeField] private Text TextFinishTimer;
    [SerializeField] private Image Fade;
    [SerializeField] private List<GameObject> Boll = new List<GameObject>();
    [SerializeField] private Image[] StartImageBoll;
    [SerializeField] private Image[] KillImageBoll;
    private Vector3 RotateImageShake;
    private int Healse = 0;
    public ParticleSystem WaterEffect;
    Sequence mysequence;
    [SerializeField] private SpriteRenderer[] CrossImage;
    [SerializeField] private Image TouchGameImage;
    [SerializeField] private GameObject ContainerLose;
    [SerializeField] private GameObject ParticleWinGame;
    public AudioSource WaterAudioSpawn;
    bool ChecFerstTouch = false;
    public GameObject SetObjectFinish
    {
        set { FinisObjectStart.Add(value); }
    }
    public GameObject SetBoll
    {
        set { Boll.Add(value); }
    }
    public Sprite GetImage
    {
        get
        {
            return BollImages[Random.Range(0, BollImages.Length)];
        }
    }
    void Start()
    {
        Fade.DOFade(0, 0.6f).OnComplete(delegate() { Fade.gameObject.SetActive(false); });
        CurrentLevels = PlayerPrefs.GetInt("Levels", 0);
        Levels[CurrentLevels].SetActive(true);
        RotateImageShake = TextFinishTimer.transform.parent.transform.position;
        TouchGameImage.DOFade(0.5f, 1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
       

        TextFinishTimer.transform.parent.transform.DORotate(new Vector3(0, 0, 0 + 10), 0.1f).OnComplete(delegate ()
        {
            TextFinishTimer.transform.parent.transform.DORotate(new Vector3(0, 0, 0 - 10), 0.1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        });
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && !ChecFerstTouch)
        {
            ChecFerstTouch = true;
            TouchGameImage.GetComponent<Image>().DOFade(0, 0.5f).OnComplete(() => { TouchGameImage.gameObject.SetActive(false); });
          
        }
    }
    public void ChecLoseGame()
    {
        if (Stay != StayGame.Lose && Stay != StayGame.Win)
        {
            Healse++;
            if (Healse >= 3)
            {
                ActiveLoseContainer();
            }
            for (int i = 0; i < Healse; i++)
            {
                CrossImage[i].gameObject.SetActive(true);
            }
        }
    }
    private void ActiveLoseContainer()
    {
        TextFinishTimer.transform.parent.gameObject.SetActive(false);
        Stay = StayGame.Lose;
        ContainerLose.SetActive(true);
        ContainerLose.GetComponent<Image>().DOFade(1, 0.6f).OnComplete(() => {
            ContainerLose.GetComponent<Image>().DOFade(1, 1.5f).OnComplete(() => { RestartGame(); });
            });
        
    }
    private void RestartGame()
    {
        Fade.gameObject.SetActive(true);
        Fade.DOFade(1, 0.6f).OnComplete(delegate ()
        {
            SceneManager.LoadScene(0);
        });
    }
    public void CheatActiveFinish()
    {
        if (FinisObjectStart.Count == AllFinishObject.Count)
        {
            if (mysequence!=null)
            mysequence.Kill();
            FinishTimer();

        }
        else
        {
            TextFinishTimer.text = "";
            if (mysequence != null)
                mysequence.Kill();

            TextFinishTimer.transform.parent.gameObject.SetActive(false);
        }

    }
    private void FinishTimer()
    {
        TextFinishTimer.transform.parent.transform.localScale = Vector3.zero;
        mysequence = DOTween.Sequence();
        TextFinishTimer.transform.parent.gameObject.SetActive(true);
        TextFinishTimer.transform.localScale = Vector3.zero;
        TextFinishTimer.text = "3";
        mysequence.Append(TextFinishTimer.transform.DOScale(2, 1).OnComplete(delegate ()
        {
            TextFinishTimer.transform.localScale = Vector3.zero;
            TextFinishTimer.text = "2";

        }));
        mysequence.Join(TextFinishTimer.transform.parent.transform.DOScale(1, 0.3f));
        mysequence.Append(TextFinishTimer.transform.DOScale(2, 1).OnComplete(delegate ()
              {
                  TextFinishTimer.transform.localScale = Vector3.zero;
                  TextFinishTimer.text = "1";
              }));
        mysequence.Append(TextFinishTimer.transform.DOScale(2, 1).OnComplete(delegate ()
                {
                    TextFinishTimer.gameObject.SetActive(false);
                    Finish();
                }));
        
    }
    
    private void Finish()
    {
        if (Stay != StayGame.Lose && Stay != StayGame.Win)
        {
            ParticleWinGame.SetActive(true);
            Stay = StayGame.Win;
            Fade.gameObject.SetActive(true);
            Sequence mySequence = DOTween.Sequence();
            mySequence.AppendInterval(1);
            mySequence.Append( Fade.DOFade(1, 0.6f).OnComplete(delegate ()
            {

                CurrentLevels++;
                if (CurrentLevels >= Levels.Length)
                {
                    PlayerPrefs.SetInt("Levels", 0);
                    SceneManager.LoadScene(0);
                }
                else
                {
                    PlayerPrefs.SetInt("Levels", CurrentLevels);
                    SceneManager.LoadScene(0);
                }
                FinisObjectStart.Clear();
                AllFinishObject.Clear();
                Boll.Clear();
                Start();

            }));
        }
    }
    public void ScaleAllBoll()
    {
        for (int i = 0; i < Boll.Count; i++)
        {
            float RandomScale = Mathf.PerlinNoise(Boll[i].transform.position.x, Boll[i].transform.position.y) / 250;
            if (Boll[i].transform.localScale.x < 1.4f)
                Boll[i].transform.localScale += new Vector3(RandomScale, RandomScale, RandomScale);
        }
        Instantiate(WaterEffect, Boll[Random.Range(0, Boll.Count)].transform.position, Quaternion.identity);
        
    }
}
