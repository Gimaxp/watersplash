﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishObject : MonoBehaviour
{
    SpriteRenderer SR;
    RaycastHit2D[] hits;
    public LayerMask LayerMask;
    public bool Active = false;
    void Start()
    {
        SR = GetComponent<SpriteRenderer>();
        GameController.Instance.SetObjectFinish = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
       hits = Physics2D.BoxCastAll(transform.position, transform.localScale, 360, Vector2.zero,0, LayerMask);
        if (hits.Length != 0)
        {
            if (!Active)
            {
           
                GameController.Instance.AllFinishObject.Add(gameObject);
                SR.color = Color.green;
                Active = true;
                GameController.Instance.CheatActiveFinish();
            }
            
        }
        else
        {
            if (Active)
            {
                GameController.Instance.AllFinishObject.Remove(gameObject);
                SR.color = Color.white;
                Active = false;
                GameController.Instance.CheatActiveFinish();
            }
        }
    }
  
}
