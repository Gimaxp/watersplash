﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BollScript : MonoBehaviour
{
    bool lose = false;
    void Start()
    {
        GetComponent<SpriteRenderer>().sprite = GameController.Instance.GetImage;
        GameController.Instance.SetBoll = gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Metaball_liquid" )
        {
            GameController.Instance.ScaleAllBoll();
            VibrationController.Instance.VibrateWithTypeSelection();
            Destroy(collision.gameObject);
        }
        if(collision.gameObject.tag == "Kill" && !lose)
        {
            GameController.Instance.ChecLoseGame();
            gameObject.GetComponent<SpriteRenderer>().color = Color.black;
            lose = true;
            VibrationController.Instance.VibrateWithTypeMEDIUMIMPACT();
        }
    }
}
