﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBolls : MonoBehaviour
{
    [SerializeField] private GameObject BollPrefabs;
    [SerializeField] private int Count;
    
    void Start()
    {
        for (int i = 0; i < Count; i++)
        {
            var boll = Instantiate(BollPrefabs, transform);
            boll.transform.position = new Vector3(boll.transform.position.x + Random.Range(-0.5f, 0.5f), boll.transform.position.y + Random.Range(0, 0.5f), 0);

        }
    }

   
    void Update()
    {
        
    }
}
