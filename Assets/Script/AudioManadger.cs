﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AudioManadger : Singleton<AudioManadger>
{
    public AudioSource Sound;
    [SerializeField] private Toggle SoundOnOrOff;
    [SerializeField] private AudioClip SoundGame;
    bool activeSound = true;
    void Start()
    {
      

        SoundOnOrOff.isOn = PlayerPrefs.GetInt("Sound", 0) == 1;
        SoundOn();
    }

    // Update is called once per frame
    void Update()
    {
       
    }
    public void PlaySound()
    {
       
    }
  
    public void SoundOn()
    {
        Sound.enabled = !SoundOnOrOff.isOn;

        if(SoundOnOrOff.isOn)
        PlayerPrefs.SetInt("Sound", 1);
        else
        PlayerPrefs.SetInt("Sound", 0);
    }
}
